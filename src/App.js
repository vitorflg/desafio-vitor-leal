import Nav from './components/nav/Nav';
import './App.css';
import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes'
import { Auth } from "aws-amplify";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuth: false,
      isAuthenticating: true
    };
  }

  // Altera o estado de autenticação do usuário
  userAuthStatus = bool => {
    this.setState({ isAuth: bool });
  }
  // Quando o componente termina de montar, verifica se existe uma sessão ativa. Se tiver, seta o isAuthenticating como false
  // e libera a renderização
  async componentDidMount() {
    try {
      await Auth.currentSession();
      this.userAuthStatus(true);
    }
    catch(error) {
      console.log(error)
    }

    this.setState({ isAuthenticating: false });
  }

  render() {

    // Passando propriedades gerais de sessão para componenetes filhos
    const childProps = {
      isAuth: this.state.isAuth,
      userAuthStatus: this.userAuthStatus
    };

    return (
      !this.state.isAuthenticating &&
      <Router>
        <div className="App">
          <Nav childProps={childProps} />
          <Routes childProps={childProps} />
        </div>
      </Router>
    );
  }
}

export default App;
