export default {
  apiGateway: {
    REGION: "us-east-1",
    URL: "https://x9oqlutna4.execute-api.us-east-1.amazonaws.com/prod"
  },
  cognito: {
    REGION: "us-east-2",
    USER_POOL_ID: "us-east-2_S04QYmjkz",
    APP_CLIENT_ID: "4vajdlgtqb5mp0obenmka4pqgh",
    IDENTITY_POOL_ID: "us-east-2:deadc5f5-e4c7-4389-8f44-5bcd43843a90"
  }
};
