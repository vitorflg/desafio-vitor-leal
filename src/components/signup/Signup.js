import React, { Component } from 'react';
import './Signup.css';
import { Auth } from "aws-amplify";
import { Link } from "react-router-dom";
import $ from "jquery";
import CustomSubmit from '../custom-submit/CustomSubmit';

class Signup extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
      confirmationCode: "",
      showPopup: false
    };
  }

  // Quando o input mudar de valor, define o email ou a senha com o valor digitado
  handleChange = (type, event) => {
    this.setState({
      [type]: event.target.value
    });
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  // preventDefault para evitar que o browser recarre pós submit.Usa o Amplify para logar, define isAuth como true e chama a history para redirecionar para home
  handleSubmit = async event => {
    event.preventDefault();

    if(this.state.password !== this.state.confirmPassword) {
      alert('Senhas não coincidem, tente novamente!');
      return;
    }

    try {
      await Auth.signUp({
        username: this.state.email,
        password: this.state.password
      });
    } catch (e) {
        alert(e.message);
        return;
    }

    await this.togglePopup();
  }

  // Verifica se o código de confirmação é válido
  handleConfirmationSubmit = async event => {
    event.preventDefault();

    try {
      await Auth.confirmSignUp(this.state.email, this.state.confirmationCode);
      await Auth.signIn(this.state.email, this.state.password);

      this.props.userAuthStatus(true);
      this.props.history.push("/");
    } catch (e) {
      alert(e.message);
    }
  }

  render() {
    return (
        <React.Fragment>
        <div className="auth signup">
          <div className="back-link">
            <Link to="/signin">Voltar</Link>
          </div>
          <div className="auth-box">
            <form onSubmit={this.handleSubmit}>
              <div className="login-title">
                <h1>Signup</h1>
              </div>
              <div className="email">
                <input value={this.state.email} onChange={this.handleChange.bind(this, 'email')} placeholder="user@email.com" type="email"></input>
              </div>
              <div className="password">
                <input value={this.state.password} onChange={this.handleChange.bind(this, 'password')} placeholder="*******" type="password"></input>
              </div>
              <div className="password">
                <input value={this.state.confirmPassword} onChange={this.handleChange.bind(this, 'confirmPassword')} placeholder="*******(confirmação)" type="password"></input>
              </div>
              <div className="submit">
                <CustomSubmit text="Cadastrar" />
              </div>
            </form>
          </div>
        </div>
        <div>
        {this.state.showPopup ?
          <div className='popup'>
            <div className='popup_inner'>
              <div className="title">
                <h1>Verifique seu e-mail!</h1>
              </div>
                <form onSubmit={this.handleConfirmationSubmit.bind(this)}>
                  <div className="confimation-code">
                    <input id="confirmation-code" value={this.state.confirmationCode} onChange={this.handleChange.bind(this, 'confirmationCode')} placeholder="Confirmation Code" type='text'></input>
                  </div>
                  <div className="save">
                    <CustomSubmit text="Confirmar"></CustomSubmit>
                  </div>
                </form>
            </div>
          </div>
          : null
          }
        </div>
        </React.Fragment>
    );
  }
}

export default Signup;
