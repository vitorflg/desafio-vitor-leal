import React, { Component } from 'react';
import './CustomSubmit.css'

class CustomSubmit extends Component {
  render() {

      return (
        <button className="custom-submit" type="submit">{this.props.text}</button>
      );
  }
}

export default CustomSubmit;
