import React, { Component } from 'react';
import './Home.css';
import { Auth } from "aws-amplify";
import $ from "jquery";
import Popup from '../popup/Popup';
import Cart from '../cart/Cart';
import CustomSubmit from '../custom-submit/CustomSubmit';
import { API } from "aws-amplify";
import loading from '../../loading.svg';
import cart from '../../images/cart.svg';
import uuidv1 from 'uuid/v1';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isLoadingName: true,
      userName: "",
      showCart: false,
      showPopup: false,
      products: [],
      cart: []
    };
  }

  async componentWillMount() {
    // Retorna o cart do localStorage
    if(localStorage.getItem('cart')) {
      let cartBrowser = await JSON.parse(localStorage.getItem('cart'));
      await this.setState({cart: cartBrowser});
    }
  }

  // Lida com o processo de adicionar o produto no carrinho e no localStorage
  handleProduct = async event => {
    event.preventDefault();
    event.persist();

    this.setState({showCart: true});

    // Detalhes do "Adicionar ao cart"
    let id = $(event.target).find(".id").text();
    let cartPid = uuidv1();
    let qtd = $(event.target).find(".qtd").val();
    let size = $(event.target).find(".size").val();
    let price = $(event.target).find(".product-price").text();
    let name = $(event.target).find(".product-name").text();
    let totalPrice = qtd * price;

    let obj2 = {"cartPid": cartPid, "productName": name, "productQtd": qtd, "productSize": size, "productPrice": totalPrice};
    let alreadyExists = false;

    await this.state.cart.map((obj,i) => {
      if(obj.productName === obj2.productName && obj.productSize === obj2.productSize) {
        alreadyExists = true;

        let products = [...this.state.cart];
        let product = {...products[i]};

        product.productQtd = parseInt(obj.productQtd) + parseInt(obj2.productQtd);

        product.productPrice = parseInt(obj.productPrice) + parseInt(obj2.productPrice);

        products[i] = product;

        this.setState({cart: products});
      }
    });

    if(alreadyExists) {
      localStorage.setItem('cart', JSON.stringify(this.state.cart));
      return;
    }

    // Criar cart
    this.setState({
      cart: [...this.state.cart, obj2]
    });

    localStorage.setItem('cart', JSON.stringify(this.state.cart));
  }

  // Esconde/Mostra o Popup
  togglePopup() {
    if(this.state.showPopup) {
      $('.App').removeClass("overflow-auto");
    }

    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  toggleCart() {
    this.setState({
      showCart: !this.state.showCart
    });
  }

  async removeAllFromCart() {
    this.setState({
      cart: ""
    });

    localStorage.setItem('cart', '');

  }

  async removeFromCart(elementId) {
    let result =  await this.state.cart.filter(obj => {
      return obj.cartPid !== elementId
    });

    this.setState({
      cart: result
    });

    localStorage.setItem('cart', JSON.stringify(this.state.cart));
  }

  //Quando for realizar o pedido e alterar o nome, enviar essa função para o componente Popup executar
  async handleUserName() {
    if(this.props.isAuth) {
      let user = await Auth.currentAuthenticatedUser()
      this.setState({ userName: user.attributes.name });
    }
  }

  // Quando o componente termina de montar, pega o nome do current_user e puxa os produtos da API
  async componentDidMount() {

    await this.handleUserName();

    this.setState({ isLoadingName: false });

    try {
      const products = await this.listProducts();

      await this.setState({ products: products });
    }
    catch (e) {
      console.log(e);
    }

    this.setState({ isLoading: false });
  }


  // Retorna lista de produtos
  listProducts() {
    return API.get("products", "/products");
  }

  // Cria um loop na lista de produtos e renderiza um componente para cada elemento
  renderProductsList(products) {
    return [{}].concat(products).map(
      (product, i) =>
        i !== 0 ?
          <div className="product">
            <form id={product.productId} onSubmit={this.handleProduct}>
              <div className="product-top">
                <div className="id">{product.productId}</div>
                <img alt="miniatura de minion" src={product.image} />
              </div>
              <div className="product-bottom">
                <div className="product-details">
                  <div className="left">
                    <span className="product-name">{product.name}</span>
                    <br/>
                  <span>R$ <strong className="product-price">{product.price}</strong></span>
                  </div>
                  <div className="right">
                    <select className="size">
                      <option value="P">P</option>
                      <option value="M">M</option>
                      <option value="G">G</option>
                    </select>
                    <select className="qtd">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                    </select>
                  </div>
                </div>
                <div className="submit checkout">
                  <CustomSubmit text="Adicionar" />
                </div>
              </div>
              </form>
          </div>

          : <div></div>
    );
  }

  render() {
    let var_welcome, products;

    //isLoading de produtos
    if(this.state.isLoading === false) {
      products = this.renderProductsList(this.state.products);
    }
    else {
      products = <img alt="loading.." className="loading loading-products" width="27px" src={loading}/>;
    }

    //isLoading do nome
    if(this.state.isLoadingName === false) {
      if(this.state.userName) {
        var_welcome = 'Bem-vindo ' + this.state.userName + "!";
      }
      else {
        var_welcome = 'Bem-vindo à nossa loja!';
      }
    }
    else {
      var_welcome = <img alt="loading.." className="loading" width="27px" src={loading}/>;
    }

    return (
      <div>
        <div className="user-intro">
          <div className="welcome">
              <h2>{var_welcome} </h2>
            <div className="product-description">
              <p>Essas miniaturas de minions estão à procura de um novo lar, dê uma chance a elas. Elas não são irritantes, nós juramos rs! Feito com &#10084; pela <strong>INVENTOS digitais</strong></p>
            </div>
          </div>
        </div>
        <div className="products">
          {products}
        </div>
        <div onClick={this.toggleCart.bind(this)} className="cart">
          <img alt="icone do cart" width="40px" src={cart}/>
        </div>
        {this.state.showCart ?
        <Cart childProps={this.props} togglePopup={this.togglePopup.bind(this)} toggleCart={this.toggleCart.bind(this)} removeFromCart={this.removeFromCart.bind(this)} removeAllFromCart={this.removeAllFromCart.bind(this)} cart={this.state.cart} />
        : null
        }

        {this.state.showPopup ?
        <Popup handleProduct={this.handleProduct.bind(this)} handleUserName={this.handleUserName.bind(this)}
          closePopup={this.togglePopup.bind(this)}
        />
        : null
        }
      </div>
    );
  }
}

export default Home;
