import React from 'react';
import './Popup.css'
import closer from '../../images/closer.svg';
import CustomSubmit from '../custom-submit/CustomSubmit';
import $ from 'jquery';
import { Auth } from "aws-amplify";

class Popup extends React.Component {

  componentDidMount() {
    $('.App').addClass("overflow-auto");
  }

  handleUserInfo = async event => {
    event.preventDefault();

    //Pega as informações passadas pelo usuário
    let name = $('#name').val();
    let cpf = $('#cpf').val();
    let adress = $('#adress').val();
    let tel = $('#tel').val();

    // Validação básica do form
    if(name.length < 1 || cpf.length < 4 || adress.length < 6 || tel.length < 7) {
      alert('É preciso preencher todas as informações!');
      return;
    }

    // Passa as informações para o cognito
    let user = await Auth.currentAuthenticatedUser();
    await Auth.updateUserAttributes(user, {
      'name': name,
      'custom:cpf': cpf,
      'custom:adress': adress,
      'custom:tel': tel
    });

    // Fecha automaticamente o popup e dá boas-vindas ao usuário
    $('.closer').click();
      $('.App').removeClass("overflow-auto");
      alert('Obrigado pelas informações, agora você pode fazer o seu pedido!');
      this.props.handleUserName();
  }

  render() {
    return (
      <div className='popup'>
        <div className='popup_inner'>
          <div className="title">
            <h1>Preencha antes de finalizar a reserva</h1>
          </div>
            <form onSubmit={this.handleUserInfo}>
              <div className="name">
                <input id="name" placeholder="Nome" type='text'></input>
              </div>
              <div className="cpf">
                <input id="cpf" placeholder="CPF" type='text'></input>
              </div>
              <div className="adress">
                <input id="adress" placeholder="Endereço completo" type='text'></input>
              </div>
              <div className="tel">
                <input id="tel" placeholder="Telefone" type='text'></input>
              </div>
              <div className="save">
                <CustomSubmit text={"Salvar"}></CustomSubmit>
              </div>
            </form>
            <div className="closer" onClick={this.props.closePopup}>
              <img alt="Fechar popup" width="30px" src={closer} />
            </div>
        </div>
      </div>
    );
  }
}

export default Popup;
