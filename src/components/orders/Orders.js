import React, { Component } from 'react';
import { Auth } from "aws-amplify";
import { Link} from "react-router-dom";
import './Orders.css'
import { API } from "aws-amplify";
import loading from '../../loading.svg';

class Orders extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      orders: [],
      user: ""
    };
  }

  // Requisita todas os pedidos do BD
  listOrders() {
    return API.get("products", "/orders");
  }

  async componentDidMount() {

    try {
      let user = await Auth.currentAuthenticatedUser();
      const orders = await this.listOrders();
      await this.setState({ orders: orders });
      await this.setState({ user: user });
    }
    catch (e) {
      console.log(e);
    }

    this.setState({ isLoading: false });
  }

  // Renderiza a interface da tabela com os dados vindos da API
  renderOrdersList(orders, user) {
    return [{}].concat(orders).map(
      (order, i) =>
        i !== 0 && user.attributes.sub === order.userId ?
          <tr>
            <td>{order.orderId || 'X'}</td>
            <td>{order.name || 'X'}</td>
            <td>{order.price || 'X'}</td>
          </tr>
          : <div></div>
    );
  }


  render() {
    let orders, initial_table;

    //isLoading de produtos
    if(this.state.isLoading === false) {
      orders = this.renderOrdersList(this.state.orders, this.state.user);
      initial_table = <table>
        <caption>Meus pedidos</caption>
        <thead>
          <tr>
            <th>Protocolo</th>
            <th>Nome</th>
            <th>Valor</th>
          </tr>
        </thead>
        <tbody>
          {orders}
        </tbody>
      </table>
    }
    else {
      initial_table = <img alt="loading.." className="loading loading-products" width="27px" src={loading}/>;
    }
    return (
      <div className="orders">
        {initial_table}
      </div>
    );
  }
}

export default Orders;
