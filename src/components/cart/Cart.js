import './Cart.css';
import React, { Component } from 'react';
import { Auth } from "aws-amplify";
import * as emailjs from 'emailjs-com';
import $ from "jquery";
import remove from '../../images/remove.svg';
import { API } from "aws-amplify";

class Cart extends Component {

  // Lida com a interface do produto no cart e suas possíveis remoções
  handleCart() {

    // Retorna a interface de cada produto no cart
    let cart = this.props.cart;

    return cart.map(
      (p, i) =>
        <h4><strong className="productId">{p.cartPid}</strong>{p.productQtd}x - {p.productName} ({p.productSize}) - R${p.productPrice}<img onClick={this.removeFromCart.bind(this)} width="23px" src={remove}/></h4>
    );
  }

  // Remove do cart a partir do contexto
  removeFromCart(event) {
      let element = event.target.parentNode.textContent.substring(0,36);
      console.log(element);

      this.props.removeFromCart(element);
  }

  // Lida com criação do pedido no BD e o envio do email
  async handleOrder() {

    // Detalhes do pedido para ser finalizado
    let orderName = "";
    let orderPrice = 0;

    this.props.cart.forEach(function(p) {
      orderName += p.productQtd + 'x ' + p.productName + '(' + p.productSize + ') ';
      orderPrice += p.productPrice;
    });

    if(orderName.length < 1) {
      alert('Adicione produtos antes de finalizar a compra!');
      return;
    }

    try {
      let user = await Auth.currentAuthenticatedUser();

      if(!user.attributes['custom:adress']) {
        this.props.togglePopup();
      }
      else {
        // Criar pedido no BD
        await this.createOrder({
           userId: user.attributes.sub,
           name: orderName,
           price: orderPrice
         });

        // Enviar e-mail
        var template_params = {
          "email": user.attributes.email,
          "to_name": user.attributes.name,
          "adress": user.attributes["custom:adress"],
          "name": orderName,
          "total_price": orderPrice,
        }

          var service_id = "default_service";
          var template_id = "template_QvECc7F8";

          emailjs .send(service_id, template_id, template_params, 'user_xT2wTlvr8vp3CkgwcchiE');

          alert('Parabéns pela compra! Enviamos um e-mail com os detalhes..');

          this.props.removeAllFromCart();
          this.props.toggleCart();
      }
    }
    catch {
      alert('Por favor, entre ou cadastre-se para começar a comprar');

      this.props.childProps.history.push('/signin');
    }
  }

  // Cria um novo pedido no BD
  createOrder(order) {
    return API.post("products", "/orders", {
      body: order
    });
  }

  render() {
     let cart;
      if(this.props.cart) {
        cart = this.handleCart();
      }
      else {
        cart = <div></div>
      }
      return (
        <div className="cart-box">
          <div className="title">
            <h1>Carrinho</h1>
          </div>
          <div className="content">
            {cart}
          </div>
          <div className="submit-order">
            <button onClick={this.handleOrder.bind(this)}>Reservar</button>
          </div>
        </div>
      );
  }
}

export default Cart;
