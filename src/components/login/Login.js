import React, { Component } from 'react';
import './Login.css';
import CustomSubmit from '../custom-submit/CustomSubmit';
import { Auth } from "aws-amplify";
import { Link } from "react-router-dom";

class Login extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
  }

  // Quando o input mudar de valor, define o email ou a senha com o valor digitado
  handleChange = (type, event) => {
    this.setState({
      [type]: event.target.value
    });
  }

  // preventDefault para evitar que o browser recarre pós submit.Usa o Amplify para logar, define isAuth como true e chama a history para redirecionar para home
  handleSubmit = async event => {
      event.preventDefault();

      Auth.signIn(this.state.email, this.state.password)
      .then((user) => {
        this.props.userAuthStatus(true);
        console.log(user.storage.name,user);
        this.props.history.push("/");
      }).catch((e) => {
         alert(e.message);
      });

  }

  render() {
    return (
      <div className="auth">
        <div className="back-link">
          <Link to="/">Voltar</Link>
        </div>
        <div className="auth-box">
          <form onSubmit={this.handleSubmit}>
            <h1>Log in</h1>
            <div className="email">
              <input value={this.state.email} onChange={this.handleChange.bind(this, 'email')} placeholder="user@email.com" type="email"></input>
            </div>
            <div className="password">
              <input value={this.state.password} onChange={this.handleChange.bind(this, 'password')} placeholder="*******" type="password"></input>
            </div>
            <div className="submit">
              <CustomSubmit  text="Enviar" />
            </div>
          </form>
          <Link to="/signup" className="signup-link">Cadastre-se</Link>
        </div>
      </div>
    );
  }
}

export default Login;
