import React, { Component } from 'react';
import { Auth } from "aws-amplify";
import minion from '../../images/minion.svg';
import { Link} from "react-router-dom";

class Nav extends Component {

  // Seta isAuth como false e chama o Amplify para deslogar
  handleLogout = async event => {
    this.props.childProps.userAuthStatus(false);
    await Auth.signOut();
  }

  render() {
    if (window.location.pathname === '/signin' || window.location.pathname === '/signup') return null;
      return (
        <header className="App-header">
          <div className="header-left">
            <div className="logo">
              <Link to="/">
                <img src={minion} className="App-logo" alt="logo" />
              </Link>
            </div>
          </div>
          <div className="header-right">
            <div className="auth-div">
              {!this.props.childProps.isAuth ?
              <React.Fragment>
                <Link to="/signin">Entrar</Link>
                <Link to="/signup">Cadastrar</Link>
              </React.Fragment> :
              <React.Fragment>
                <Link to="/orders">Meus pedidos</Link>
                <a className="logout" onClick={this.handleLogout}>Sair</a>
              </React.Fragment>
            }
            </div>

          </div>
        </header>
      );
  }
}

export default Nav;
