import React from "react";
import { Switch } from "react-router-dom";
import Home from "./components/home/Home";
import Login from "./components/login/Login";
import Orders from "./components/orders/Orders"
import Signup from "./components/signup/Signup";
import AppliedRoute from "./components/AppliedRoute/AppliedRoute";

export default ({ childProps }) =>
  <Switch>
    <AppliedRoute path="/" exact={true} component={Home} props={childProps} />
    <AppliedRoute path="/signin" component={Login} props={childProps} />
    <AppliedRoute path="/signup" exact={true} component={Signup} props={childProps} />
    <AppliedRoute path="/orders" exact={true} component={Orders} props={childProps} />
  </Switch>
